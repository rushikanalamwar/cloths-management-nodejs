var express = require('express')
var clothController = require('../controllers/cloths.controller')
const router = express.Router()

router.post('/add', clothController.uploadImg, clothController.addCloths)
// router.post('/add', clothController.addCloths)

router.post('/update/:id', clothController.uploadImg, clothController.updateCloth)
router.post('/delete/:id',   clothController.deleteCloth)
router.get('/', clothController.showCloths)
router.get('/:id', clothController.showSingleCloth)


module.exports = router;