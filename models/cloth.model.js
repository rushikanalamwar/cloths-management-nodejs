var mongoose = require('mongoose')
const Schema = mongoose.Schema;

var clothSchema = new Schema({
    name:{
        type: String,
        required: true      
    },
    description:{
        type: String,
        required: true 
    },
    price:{
        type: String,
    },
    discountPrice:{
        type: String,
    },
    discountPer:{
        type: String,
    },
    clothImage:{
        type: String
    }
});

module.exports = mongoose.model('cloth', clothSchema);
