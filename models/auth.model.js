var mongoose = require('mongoose')
const Schema = mongoose.Schema;

var nirmiteeAuth = new Schema({
    email:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    username:{
        type: String,
        required: true
    }
});

module.exports = mongoose.model('auth', nirmiteeAuth);
