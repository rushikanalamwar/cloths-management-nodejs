var jwt = require('jsonwebtoken')
var bcrypt = require('bcryptjs')
var auth = require("../models/auth.model")
var HttpResponse = require("../middlewares/http-response")


//signup here
const signup = async(req, res)=>{
    const {username , password, email}= req.body

    let existingUser;
    try{
        existingUser = await auth.findOne({
            email: email
        });
    }
    catch(err){
        const error = new HttpResponse(
            'something went wrong', 500
        );
        return res.status(500).json({
            Response: error
        })
    }


    if(existingUser){
        return res.status(200).json({
            response: "Email Alrady Exists, Please Login instead"
        })
    }

    //creating a hashed password and saving the user into mongo.
    let hashedpassword;
    try {
        hashedpassword = await bcrypt.hash(password, 12)
    }
    catch(err){
        const error = new HttpResponse(
            "hashing failed..", 500
        );
        return res.status(500).json({
            response: error
        }) 
    }

    //creating new user
    const newUser = new auth({
        username,
        password: hashedpassword,
        email
    });
    try{
        await newUser.save();
    }
    catch(err){
        const error = new HttpResponse("failed to save", 500);
        return res.status(500).json({response: error});
    }

    //generating json web token 
    let token;
    try{
        token = jwt.sign({
            email: newUser.email,
            password: newUser.password,
            username: newUser.username
        },
        "this is nirmitee private key",
        { expiresIn: "1h" }
        )
    }
    catch(err){
        const error = new HttpResponse("token generation failed", 500)
        return res.status(500).json({response: error})
    }

    res.status(201).json({
        userId: newUser.id,
        email: newUser.email,
        password: newUser.password,
        username: newUser.username,
        token: token
    });
};


//login here
var login = async(req, res)=>{
    const{email, username, password}= req.body;

    // checking if email exists in database
    let existingUser
    try{
        existingUser = await auth.findOne({email: email})
     }
     catch(err){
         const error = new HttpResponse("cannot find user", 500)
         return res.status(500).json({response: error})
    }
    
    if (!existingUser) {
        const error = new HttpResponse("Email not found, Please SignUp.",401);
        return res.status(500).json({ response: error });
    }

    //comparing passwords
    let isValidPassword
    try{
        isValidPassword = await bcrypt.compare(password, existingUser.password)
    }
    catch(err){
        const error = new HttpResponse("failed to compare passwords", 500)
        return res.status(500).json({response: error})
    }

    if (!isValidPassword) {
        const error = new HttpResponse("Wrong password entered", 401);
        return res.status(401).json({ response: error });
    }

    //generating token here do not touch 
    let token;
    try {
        token = jwt.sign(
        {
            userId: existingUser.id,
            username: existingUser.username,
            email: existingUser.email
            
        },
        "this is nirmitee private key",
        { expiresIn: "1h" }
        );
    } catch (err) {
        const error = new HttpResponse(
        "Token generation failed, Login not done",
        500
        );
        return res.status(500).json({ response: error });
    }
    res.json({
        userId: existingUser.id,
        username: existingUser.username,
        email: existingUser.email,
        password: existingUser.password,
        token: token,
    });
};

exports.signup= signup;
exports.login = login;
