const multer = require('multer');
const fs = require('fs')
const HttpResponse = require('../middlewares/http-response');
const cloth = require('../models/cloth.model')


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads');
      },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
const uploadImg = multer({storage: storage}).single('clothImage');



const addCloths = async(req, res)=>{

    const newcloth = new cloth({
        name:req.body.name,
        price:req.body.price,
        discountPer:req.body.discountPer,
        discountPrice: req.body.discountPrice,
        description:req.body.description,
        clothImage: req.file.path
    })
    
    try{
        await newcloth.save();
    }
    catch(err){
        const error = new HttpResponse('failed to save cloths', 500)
        return res.status(401).json({response: error})
    }

    res.status(201).json({
        clothId: newcloth.id,
        name: newcloth.name,
        price: newcloth.price,
        discountPer: newcloth.discountPer,
        discountPrice: newcloth.discountPrice,
        description: newcloth.description,
        clothImage: newcloth.clothImage
    });
};

const updateCloth = async(req, res)=>{
    const{name, price, discountPer, discountPrice, description}= req.body
    // const{clothImage}= req.file.path

    try{
       singlecloth = await cloth.updateOne({_id: req.params.id},{
            $set:{
                name: name,
                price: price,
                discountPer: discountPer,
                discountPrice: discountPrice,
                description: description,
                clothImage: req.file.path
            }
        })
    }
    catch(err){
        const error = new HttpResponse('failed to update', 500)
        return res.status(500).json({
            response: error
        })
    }
    return res.json({
        response: singlecloth
    });
};

const showCloths = async(req, res)=>{
    let allcloths
    try {
        allcloths = await cloth.find({})
    } catch (err) {
        const error = new HttpResponse('failed to display list', 500)
        return res.status(500).json({response: error})
    }
    return res.json({ result: allcloths });
}

const showSingleCloth = async(req, res)=>{
    
    try {
        singleCloth = await cloth.find({_id: req.params.id})
    } catch (err) {
        const error = new HttpResponse('failed to display item', 500)
        return res.status(500).json({response: error})
    }
    return res.json({ result: singleCloth });
}

const deleteCloth = async(req, res)=>{
    // await uploadToRemoteBucket(req.file.path)
    // await unlinkAsync(req.file.path)
    try{
        deleteSingleCloth = await cloth.findByIdAndDelete({_id:req.params.id })       
    }
    catch(err){
        const error = new HttpResponse('failed to delete cloth item', 500)
        return res.status(500).json({response: error})
    }
    return res.status(200).json({
        response: deleteSingleCloth
    })
}


exports.addCloths = addCloths;
exports.uploadImg = uploadImg;
exports.updateCloth = updateCloth;
exports.showCloths = showCloths;
exports.showSingleCloth = showSingleCloth;
exports.deleteCloth = deleteCloth;