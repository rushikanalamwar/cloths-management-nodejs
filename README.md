# CLOTHS MANAGEMENT- NODEJS
This project is a node server project built using `express` and `mongodb` to act as a backend service for cloths management.

## How to Contribute
Please follow the following flow when working and submitting features.

- Clone the project if you haven't already.
- Use `git checkout master` to switch to master branch in case you are not on `master` branch
- Always use `git pull` to get the latest project files before starting.
- Install the needed libraries for working of this project(mainly nodemon).
- run the command 'nodemon' to start the server. let the database and server connect.

## How to Check Working Of APIs
- File named documentation.http is included to test and run the login and registartion APIs.
- Its recommended to use postman for testing APIs of Cloths CRUD operation as it contains image uploading.
## NOTE
- While testing cloths CRUD APIs in postman please switch to header section and uncheck the content-type check-box as this causes intruption while using multer.

